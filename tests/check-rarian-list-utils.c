
#include "rarian-list-utils.h"
#include <check.h>
#include <stdlib.h>

typedef struct _Element Element;

struct _Element {
    Element *next;
    Element *prev;
};

static Element *element_new() { return calloc(1, sizeof(Element)); }

START_TEST(test_rrn_list_basic) {
    Element *lcl = NULL;
    Element *first = element_new();
    Element *second = element_new();

    RRN_LIST_PREPEND(lcl, first);

    ck_assert_ptr_eq(lcl, first);

    RRN_LIST_PREPEND(lcl, second);

    ck_assert_ptr_eq(lcl, second);
    ck_assert_ptr_eq(second, first->prev);
    ck_assert_ptr_eq(first, second->next);
    ck_assert_ptr_null(first->next);
    ck_assert_ptr_null(second->prev);

    RRN_LIST_REMOVE(lcl, first);
    ck_assert_ptr_eq(lcl, second);
    ck_assert_ptr_null(first->next);
    ck_assert_ptr_null(first->prev);
    ck_assert_ptr_null(second->next);
    ck_assert_ptr_null(second->prev);

    RRN_LIST_REMOVE(lcl, second);
    ck_assert_ptr_null(lcl);

    free(first);
    free(second);
}
END_TEST

START_TEST(test_rrn_list_replace) {
    Element *lcl = NULL;
    Element *first = element_new();
    Element *second = element_new();
    Element *third = element_new();
    Element *repl = element_new();

    RRN_LIST_PREPEND(lcl, third);
    RRN_LIST_PREPEND(lcl, second);
    RRN_LIST_PREPEND(lcl, first);

    ck_assert_ptr_eq(first, second->prev);
    ck_assert_ptr_eq(third, second->next);

    /* Replace the middle element. */
    RRN_LIST_REPLACE(lcl, second, repl);
    ck_assert_ptr_null(second->prev);
    ck_assert_ptr_null(second->next);

    ck_assert_ptr_eq(first, repl->prev);
    ck_assert_ptr_eq(third, repl->next);

    /* Replace the element at the head of the list. */
    RRN_LIST_REPLACE(lcl, first, second);
    ck_assert_ptr_eq(lcl, second);
    ck_assert_ptr_null(second->prev);
    ck_assert_ptr_eq(repl, second->next);

    RRN_LIST_FREE(lcl, free);
    free(first);
}
END_TEST

Suite *rarian_list_utils_suite() {
    Suite *s = suite_create("RarianListUtils");
    TCase *tc_rrn_list = tcase_create("List");

    tcase_add_test(tc_rrn_list, test_rrn_list_basic);
    tcase_add_test(tc_rrn_list, test_rrn_list_replace);
    suite_add_tcase(s, tc_rrn_list);

    return s;
}
