#define I_KNOW_RARIAN_0_8_IS_UNSTABLE
#include "check-rarian.h"
#include "rarian-man.h"
#include <check.h>
#include <string.h>

/* It should be pretty safe that the manpage for man should exist. */
#define TEST_MAN_NAME "man"
#define TEST_MAN_MAIN_CAT "7"
#define TEST_MAN_LINK_CAT "1"

START_TEST(test_rrn_man_categories) {
    char **cats = rrn_man_get_categories();
    ck_assert_ptr_nonnull(cats);
    /* Not really much to check here, just a quick sanity. */
    ck_assert_str_eq(cats[0], "1");

    rrn_man_shutdown();
}
END_TEST

struct ForEachData {
    char *stop_name;
    RrnManEntry *found_entry;
};

static int test_foreach(void *reg, void *data) {
    RrnManEntry *entry = (RrnManEntry *)reg;
    struct ForEachData *ctx = (struct ForEachData *)data;
    if (ctx->found_entry)
        return FALSE;

    if (!strcmp(entry->name, ctx->stop_name)) {
        ctx->found_entry = entry;
        return FALSE;
    }
    return TRUE;
}

START_TEST(test_rrn_man_foreach) {
    struct ForEachData ctx;
    ctx.stop_name = TEST_MAN_NAME;
    ctx.found_entry = NULL;

    rrn_man_for_each(test_foreach, &ctx);

    ck_assert_ptr_nonnull(ctx.found_entry);
    ck_assert_str_eq(ctx.found_entry->name, TEST_MAN_NAME);
    /* Man should have a link entry in category 1 */
    ck_assert_str_eq(ctx.found_entry->section, TEST_MAN_LINK_CAT);

    /* man shouldn't be in category 3. */
    ctx.found_entry = NULL;
    rrn_man_for_each_in_category("3", test_foreach, &ctx);
    ck_assert_ptr_null(ctx.found_entry);

    /* man should be in category 7. */
    ctx.found_entry = NULL;
    rrn_man_for_each_in_category(TEST_MAN_MAIN_CAT, test_foreach, &ctx);
    ck_assert_ptr_nonnull(ctx.found_entry);
    ck_assert_str_eq(ctx.found_entry->name, TEST_MAN_NAME);
    ck_assert_str_eq(ctx.found_entry->section, TEST_MAN_MAIN_CAT);

    /* Go through the full list for a bogus name */
    ctx.stop_name = "thishastobeabogusmanpage";
    ctx.found_entry = NULL;
    rrn_man_for_each(test_foreach, &ctx);
    ck_assert_ptr_null(ctx.found_entry);
    rrn_man_shutdown();
}

START_TEST(test_rrn_man_from_name) {
    RrnManEntry *entry;

    entry = rrn_man_find_from_name(TEST_MAN_NAME, NULL);
    ck_assert_ptr_nonnull(entry);
    ck_assert_str_eq(entry->name, TEST_MAN_NAME);
    ck_assert_str_eq(entry->section, TEST_MAN_LINK_CAT);

    entry = rrn_man_find_from_name(TEST_MAN_NAME, TEST_MAN_MAIN_CAT);
    ck_assert_ptr_nonnull(entry);
    ck_assert_str_eq(entry->name, TEST_MAN_NAME);
    ck_assert_str_eq(entry->section, TEST_MAN_MAIN_CAT);
    rrn_man_shutdown();
}
END_TEST

START_TEST(test_rrn_man_shutdown) {

    /* Test this is safe to do (not yet initialized). */
    rrn_man_shutdown();

    /* Make sure things get initialized. */
    char **cats = rrn_man_get_categories();

    /* Run twice to make sure it is well behaved. */
    rrn_man_shutdown();
    rrn_man_shutdown();
}
END_TEST

Suite *rarian_man_suite() {
    Suite *s = suite_create("RarianMan");
    TCase *tc_rrn_reg = tcase_create("RrnMan");

    tcase_add_test(tc_rrn_reg, test_rrn_man_categories);
    tcase_add_test(tc_rrn_reg, test_rrn_man_foreach);
    tcase_add_test(tc_rrn_reg, test_rrn_man_from_name);
    tcase_add_test(tc_rrn_reg, test_rrn_man_shutdown);
    suite_add_tcase(s, tc_rrn_reg);

    return s;
}
