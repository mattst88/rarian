#define I_KNOW_RARIAN_0_8_IS_UNSTABLE
#include "check-rarian.h"
#include "rarian-info.h"
#include "rarian-utils.h"
#include <check.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

START_TEST(test_rrn_info_categories) {
    char **cats = rrn_info_get_categories();

    /* There isn't much that can be reliably asserted here as far as specific
     * content, as it depends on what the system has installed. Still there
     * should be *at least* one category, so assert that. */
    ck_assert_ptr_nonnull(cats);
    ck_assert_ptr_nonnull(*cats);

    rrn_info_shutdown();
}
END_TEST

static int assert_entry(void *reg, void *data) {
    RrnInfoEntry *entry = reg;
    /* These are all the basic things which must exist. */
    ck_assert_ptr_nonnull(entry->name);
    ck_assert_ptr_nonnull(entry->base_path);
    ck_assert_ptr_nonnull(entry->base_filename);
    ck_assert_ptr_nonnull(entry->category);
    /* The base filename should start with the base_path. */
    ck_assert_ptr_eq(strstr(entry->base_filename, entry->base_path),
                     entry->base_filename);
    return TRUE;
}

START_TEST(test_rrn_info_foreach) {
    rrn_info_for_each(assert_entry, NULL);
    rrn_info_shutdown();
}
END_TEST

static int pick_first_entry(void *reg, void *data) {
    RrnInfoEntry **dest = data;
    *dest = reg;
    return FALSE;
}

static int pick_last_entry(void *reg, void *data) {
    RrnInfoEntry **dest = data;
    *dest = reg;
    return TRUE;
}

typedef struct SearchContext {
    RrnInfoEntry *entry;
    int found;
} SearchContext;

static int search_entry(void *reg, void *data) {
    RrnInfoEntry *this = reg;
    SearchContext *ctx = data;
    if (this == ctx->entry) {
        ctx->found = TRUE;
        return FALSE;
    }

    return TRUE;
}

START_TEST(test_rrn_info_search) {
    SearchContext ctx;

    ctx.entry = NULL;
    ctx.found = FALSE;
    rrn_info_for_each(pick_first_entry, &ctx.entry);
    ck_assert_ptr_nonnull(ctx.entry);
    rrn_info_for_each_in_category(ctx.entry->category, search_entry, &ctx);
    ck_assert_int_eq(ctx.found, TRUE);

    ck_assert_ptr_eq(
        ctx.entry, rrn_info_find_from_uri(ctx.entry->name, ctx.entry->section));

    ctx.entry = NULL;
    ctx.found = FALSE;
    rrn_info_for_each(pick_last_entry, &ctx.entry);
    ck_assert_ptr_nonnull(ctx.entry);
    rrn_info_for_each_in_category(ctx.entry->category, search_entry, &ctx);
    ck_assert_int_eq(ctx.found, TRUE);

    ck_assert_ptr_eq(
        ctx.entry, rrn_info_find_from_uri(ctx.entry->name, ctx.entry->section));

    rrn_info_shutdown();
}

Suite *rarian_info_suite() {
    Suite *s = suite_create("RarianInfo");
    TCase *tc_rrn_reg = tcase_create("RrnInfo");

    tcase_add_test(tc_rrn_reg, test_rrn_info_categories);
    tcase_add_test(tc_rrn_reg, test_rrn_info_foreach);
    tcase_add_test(tc_rrn_reg, test_rrn_info_search);
    suite_add_tcase(s, tc_rrn_reg);

    return s;
}
