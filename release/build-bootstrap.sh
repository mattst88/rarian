#!/bin/bash

set -xe

# Clear the tsflags config so that docs are installed.
dnf install -y \
    --setopt='tsflags=' \
    autoconf \
    automake \
    bzip2 \
    check-devel \
    diffutils \
    info \
    git \
    gcc \
    gcc-c++ \
    libtool \
    make \
    man-db \
    man-pages

