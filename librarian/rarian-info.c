/*
 * rarian-info.c
 * This file is part of Rarian
 *
 * Copyright (C) 2007 - Don Scorgie
 *
 * Rarian is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * Rarian is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <stdio.h>
#include <string.h>

#include "config.h"
#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif /*HAVE_MALLOC_H*/
#include <ctype.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "rarian-info.h"
#include "rarian-list-utils.h"
#include "rarian-utils.h"

typedef struct _InfoLink InfoLink;

struct _InfoLink {
    RrnInfoEntry *reg;
    InfoLink *next;
    InfoLink *prev;
};

static InfoLink *info_head = NULL;
static InfoLink *info_tail = NULL;
static char **categories = NULL;

static char *set_category(const char *new_cat) {
    char *stripped = strdup(new_cat);
    char **cats = categories;
    int ncats = 1;

    stripped = rrn_strip(stripped);
    while (cats && *cats) {
        if (!strcmp(stripped, *cats)) {
            return stripped;
        }
        ncats++;
        cats++;
    }
    /* If we get here, we got a new category and have to do some work */
    categories = realloc(categories, sizeof(char *) * (ncats + 1));
    if (!categories)
        abort();

    categories[ncats - 1] = strdup(stripped);
    categories[ncats] = NULL;
    return stripped;
}

static void process_initial_entry(const char *line,
                                  RrnInfoEntry *current_entry) {
    char *tmp = (char *)line;
    char *end_name = NULL;
    char *begin_fname = NULL;
    char *end_fname = NULL;
    char *begin_section = NULL;
    char *end_section = NULL;
    char *comment = NULL;

    if (!tmp) {
        fprintf(stderr, "Error: Malformed line!  Ignoring\n");
        return;
    }

    tmp++;
    end_name = strchr(tmp, ':');
    if (!end_name) {
        fprintf(stderr, "Error: Malformed line (no ':').  Ignoring entry\n");
        return;
    }
    begin_fname = strchr(end_name, '(');
    if (!begin_fname) {
        fprintf(stderr,
                "Error: Malformed line (no filename).  Ignoring entry\n");
        return;
    }
    end_fname = strchr(begin_fname, ')');
    if (!end_fname) {
        fprintf(stderr,
                "Error: Malformed line (no filename close).  Ignoring entry\n");
        return;
    }
    end_section = strchr(end_fname, '.');

    if (!end_section) {
        fprintf(stderr,
                "Error: Malformed line (no section).  Ignoring entry\n");
        return;
    }

    current_entry->base_filename = NULL;
    current_entry->doc_name = rrn_strip(rrn_strndup(tmp, (end_name - tmp)));
    current_entry->name =
        rrn_strip(rrn_strndup(begin_fname + 1, (end_fname - begin_fname - 1)));
    if (end_section == end_fname + 1)
        current_entry->section = NULL;
    else {
        current_entry->section = rrn_strip(
            rrn_strndup(end_fname + 1, (end_section - end_fname - 1)));
    }
    comment = rrn_strip(strdup(end_section + 1));
    if (strlen(comment) > 0) {
        current_entry->comment = comment;
    } else {
        free(comment);
        current_entry->comment = NULL;
    }
}

static void process_add_desc(const char *line, RrnInfoEntry *current_entry) {
    char *current;
    char *cpy = NULL;
    cpy = rrn_strip(strdup(line));
    if (!cpy)
        return;

    if (current_entry->comment) {
        current = rrn_strconcat(current_entry->comment, " ", cpy, NULL);
        free(current_entry->comment);
    } else {
        current = strdup(cpy);
    }
    current_entry->comment = current;
    free(cpy);
}

static int process_check_file(RrnInfoEntry *current_entry) {
    char *filename = NULL;
    char *tmp = NULL;
    InfoLink *iter;
    struct stat fileinfo;

    if (!current_entry->name) {
        return FALSE;
    }

    /* First, look for an additional part on the filename */
    tmp = strchr(current_entry->name, '/');
    if (tmp) {
        char *new_base = NULL;
        char *addition;
        char *name;
        addition = rrn_strndup(current_entry->name, tmp - current_entry->name);
        name = strdup(tmp + 1);
        new_base = malloc(sizeof(char) * (strlen(current_entry->base_path) +
                                          strlen(addition) + 2));
        sprintf(new_base, "%s/%s", current_entry->base_path, addition);
        free(current_entry->base_path);
        free(current_entry->name);
        free(addition);
        current_entry->base_path = new_base;
        current_entry->name = name;
    }

    /* Search for duplicate files.  If we find one, we
     * use the older one and forget this one
     */
    iter = info_head;

    while (iter) {
        if (!strcmp(iter->reg->doc_name, current_entry->doc_name)) {
            return FALSE;
        }
        iter = iter->next;
    }

    /* Search for all the types we know of in all the
     * locations we know of to find the file, starting with
     * the most popular and working down.
     * If and when we find it, we set the encoding
     * (loose) and return */
    /* Use the largest possible storage for filename */
    filename = malloc(sizeof(char) * (strlen(current_entry->base_path) +
                                      (strlen(current_entry->name) * 2) + 15));

    sprintf(filename, "%s/%s.info.gz", current_entry->base_path,
            current_entry->name);
    if (!stat(filename, &fileinfo)) {
        current_entry->compression = INFO_ENCODING_GZIP;
        current_entry->base_filename = filename;
        return TRUE;
    }
    sprintf(filename, "%s/%s.gz", current_entry->base_path,
            current_entry->name);
    if (!stat(filename, &fileinfo)) {
        current_entry->compression = INFO_ENCODING_GZIP;
        current_entry->base_filename = filename;
        return TRUE;
    }
    sprintf(filename, "%s/%s.info.bz2", current_entry->base_path,
            current_entry->name);
    if (!stat(filename, &fileinfo)) {
        current_entry->compression = INFO_ENCODING_BZIP;
        current_entry->base_filename = filename;
        return TRUE;
    }
    sprintf(filename, "%s/%s.bz2", current_entry->base_path,
            current_entry->name);
    if (!stat(filename, &fileinfo)) {
        current_entry->compression = INFO_ENCODING_BZIP;
        current_entry->base_filename = filename;
        return TRUE;
    }
    sprintf(filename, "%s/%s.info.lzma", current_entry->base_path,
            current_entry->name);
    if (!stat(filename, &fileinfo)) {
        current_entry->compression = INFO_ENCODING_LZMA;
        current_entry->base_filename = filename;
        return TRUE;
    }
    sprintf(filename, "%s/%s.lzma", current_entry->base_path,
            current_entry->name);
    if (!stat(filename, &fileinfo)) {
        current_entry->compression = INFO_ENCODING_LZMA;
        current_entry->base_filename = filename;
        return TRUE;
    }
    sprintf(filename, "%s/%s.info", current_entry->base_path,
            current_entry->name);
    if (!stat(filename, &fileinfo)) {
        current_entry->compression = INFO_ENCODING_NONE;
        current_entry->base_filename = filename;
        return TRUE;
    }
    sprintf(filename, "%s/%s/%s.info.gz", current_entry->base_path,
            current_entry->name, current_entry->name);
    if (!stat(filename, &fileinfo)) {
        /* Add to base path */
        char *new_base =
            malloc(sizeof(char) * (strlen(current_entry->base_path) +
                                   (strlen(current_entry->name) * 2) + 2));
        sprintf(new_base, "%s/%s", current_entry->base_path,
                current_entry->name);
        free(current_entry->base_path);
        current_entry->base_path = new_base;

        current_entry->compression = INFO_ENCODING_GZIP;
        current_entry->base_filename = filename;
        return TRUE;
    }
    sprintf(filename, "%s/%s/%s.gz", current_entry->base_path,
            current_entry->name, current_entry->name);
    if (!stat(filename, &fileinfo)) {
        /* Add to base path */
        char *new_base =
            malloc(sizeof(char) * (strlen(current_entry->base_path) +
                                   (strlen(current_entry->name) * 2) + 2));
        sprintf(new_base, "%s/%s", current_entry->base_path,
                current_entry->name);
        free(current_entry->base_path);
        current_entry->base_path = new_base;

        current_entry->compression = INFO_ENCODING_GZIP;
        current_entry->base_filename = filename;
        return TRUE;
    }
    sprintf(filename, "%s/%s/%s.info.bz2", current_entry->base_path,
            current_entry->name, current_entry->name);
    if (!stat(filename, &fileinfo)) {
        /* Add to base path */
        char *new_base =
            malloc(sizeof(char) * (strlen(current_entry->base_path) +
                                   (strlen(current_entry->name) * 2) + 2));
        sprintf(new_base, "%s/%s", current_entry->base_path,
                current_entry->name);
        free(current_entry->base_path);
        current_entry->base_path = new_base;

        current_entry->compression = INFO_ENCODING_BZIP;
        current_entry->base_filename = filename;
        return TRUE;
    }
    sprintf(filename, "%s/%s/%s.bz2", current_entry->base_path,
            current_entry->name, current_entry->name);
    if (!stat(filename, &fileinfo)) {
        /* Add to base path */
        char *new_base =
            malloc(sizeof(char) * (strlen(current_entry->base_path) +
                                   (strlen(current_entry->name) * 2) + 2));
        sprintf(new_base, "%s/%s", current_entry->base_path,
                current_entry->name);
        free(current_entry->base_path);
        current_entry->base_path = new_base;

        current_entry->compression = INFO_ENCODING_BZIP;
        current_entry->base_filename = filename;
        return TRUE;
    }
    sprintf(filename, "%s/%s/%s.info.lzma", current_entry->base_path,
            current_entry->name, current_entry->name);
    if (!stat(filename, &fileinfo)) {
        /* Add to base path */
        char *new_base =
            malloc(sizeof(char) * (strlen(current_entry->base_path) +
                                   (strlen(current_entry->name) * 2) + 2));
        sprintf(new_base, "%s/%s", current_entry->base_path,
                current_entry->name);
        free(current_entry->base_path);
        current_entry->base_path = new_base;

        current_entry->compression = INFO_ENCODING_LZMA;
        current_entry->base_filename = filename;
        return TRUE;
    }

    sprintf(filename, "%s/%s/%s.lzma", current_entry->base_path,
            current_entry->name, current_entry->name);
    if (!stat(filename, &fileinfo)) {
        /* Add to base path */
        char *new_base =
            malloc(sizeof(char) * (strlen(current_entry->base_path) +
                                   (strlen(current_entry->name) * 2) + 2));
        sprintf(new_base, "%s/%s", current_entry->base_path,
                current_entry->name);
        free(current_entry->base_path);
        current_entry->base_path = new_base;

        current_entry->compression = INFO_ENCODING_LZMA;
        current_entry->base_filename = filename;
        return TRUE;
    }

    sprintf(filename, "%s/%s/%s.info", current_entry->base_path,
            current_entry->name, current_entry->name);
    if (!stat(filename, &fileinfo)) {
        /* Add to base path */
        char *new_base =
            malloc(sizeof(char) * (strlen(current_entry->base_path) +
                                   (strlen(current_entry->name) * 2) + 2));
        sprintf(new_base, "%s/%s", current_entry->base_path,
                current_entry->name);
        free(current_entry->base_path);
        current_entry->base_path = new_base;

        current_entry->compression = INFO_ENCODING_NONE;
        current_entry->base_filename = filename;
        return TRUE;
    }
    free(filename);
    return FALSE;
}

static RrnInfoEntry *new_entry(const char *category, const char *path) {
    RrnInfoEntry *entry = malloc(sizeof(RrnInfoEntry));
    entry->name = NULL;
    entry->base_path = strdup(path);
    entry->base_filename = NULL;
    entry->category = strdup(category);
    entry->section = NULL;
    entry->doc_name = NULL;
    entry->comment = NULL;

    return entry;
}

static void free_entry(RrnInfoEntry *entry) {
    free(entry->name);
    free(entry->base_path);
    free(entry->base_filename);
    free(entry->category);
    free(entry->section);
    free(entry->doc_name);
    free(entry->comment);
    free(entry);
}

static void process_add_entry(RrnInfoEntry *current_entry) {
    InfoLink *link = NULL;

    link = malloc(sizeof(InfoLink));
    link->reg = current_entry;
    link->next = NULL;
    link->prev = NULL;
    if (info_tail && info_head) {
        info_tail->next = link;
        link->prev = info_tail;
        info_tail = link;
    } else {
        /* Initial link */
        info_head = info_tail = link;
    }
}

static void process_info_dir(const char *dir) {
    char *filename;
    FILE *fp = NULL;
    char *line = NULL;
    size_t buf_len = 0;
    int started = FALSE;
    int ret = FALSE;
    char *current_category = NULL;
    RrnInfoEntry *current_entry = NULL;

    filename = rrn_strconcat(dir, "/dir", NULL);

    fp = fopen(filename, "r");
    if (!fp) {
        free(filename);
        return;
    }
    while (rrn_read_line(&line, &buf_len, fp)) {
        if (!started) {
            if (!strncmp(line, "* Menu", 6) || !strncmp(line, "* menu", 6)) {
                started = TRUE;
            }
            continue;
        }
        if ((*line != '*') && !isspace(*line)) {
            /* New category */
            current_category = set_category(line);
        } else if (*line == '*') {
            if (!current_category) {
                /* The docs are appearing before the first category.  Ignore
                   them. */
                fprintf(stderr,
                        "Error: Documents without categories.  Ignoring\n");
                continue;
            }

            /* New entry */
            if (current_entry) {
                if (process_check_file(current_entry)) {
                    process_add_entry(current_entry);
                } else {
                    free_entry(current_entry);
                }
                current_entry = NULL;
            }
            current_entry = new_entry(current_category, dir);

            process_initial_entry(line, current_entry);

        } else if (strlen(line) > 1 && current_category) {
            /* Continuation of description */
            process_add_desc(line, current_entry);
        } else {
            /* Blank line, ignore */
        }
    }
    if (process_check_file(current_entry)) {
        process_add_entry(current_entry);
    } else {
        free_entry(current_entry);
    }
    current_entry = NULL;
    free(line);
    fclose(fp);
    free(filename);
}

static void sanity_check_categories() {
    char **new_cats = NULL;
    int ncats = 0;

    for (char **iter = categories; iter && *iter; iter++) {
        for (InfoLink *l = info_head; l; l = l->next) {
            /* Copy over any categories actually found in the info entries to
             * the new category list. */
            if (!strcmp(l->reg->category, *iter)) {
                ncats++;
                new_cats = realloc(new_cats, sizeof(char *) * (ncats + 1));
                if (!new_cats)
                    abort();
                new_cats[ncats - 1] = strdup(*iter);
                new_cats[ncats] = NULL;
                break;
            }
        }
    }
    rrn_freev(categories);
    categories = new_cats;
}

static void rrn_info_init(void) {
    const char *default_dirs =
        "/usr/info:/usr/share/info:/usr/local/info:/usr/local/share/info";
    const char *info_dirs = NULL;
    const char *split = NULL;

    if (categories)
        return;

    info_dirs = getenv("INFOPATH");

    if (!info_dirs || !strcmp(info_dirs, "")) {
        info_dirs = default_dirs;
    }

    char **dirs = rrn_str_split(info_dirs, ':');
    for (char **dir = dirs; dir && *dir; dir++) {
        process_info_dir(*dir);
    }
    rrn_freev(dirs);

    /* Sanity check our categories.  Yes, I put this
     * comment in then came up with the function name */
    sanity_check_categories();
}

char **rrn_info_get_categories(void) {
    rrn_info_init();
    return categories;
}

void rrn_info_for_each(RrnInfoForeachFunc funct, void *user_data) {
    rrn_info_init();
    for (InfoLink *l = info_head; l; l = l->next) {
        int res;
        res = funct(l->reg, user_data);
        if (res == FALSE)
            break;
    }
    return;
}

void rrn_info_for_each_in_category(char *category, RrnInfoForeachFunc funct,
                                   void *user_data) {
    rrn_info_init();
    for (InfoLink *l = info_head; l; l = l->next) {
        int res;
        if (!strcmp(l->reg->category, category)) {
            res = funct(l->reg, user_data);
            if (res == FALSE)
                break;
        }
    }
    return;
}

RrnInfoEntry *rrn_info_find_from_uri(char *uri, char *section) {
    InfoLink *best_result = NULL;
    rrn_info_init();

    for (InfoLink *l = info_head; l; l = l->next) {
        if ((l->reg->doc_name && !strcmp(uri, l->reg->doc_name)) ||
            (!strcmp(uri, l->reg->name))) {
            if (!section || (*section && l->reg->section &&
                             !strcmp(l->reg->section, section))) {
                return l->reg;
            } else {
                best_result = l;
            }
        }
    }

    if (best_result)
        return best_result->reg;

    return NULL;
}

static void free_info_link(InfoLink *link) {
    free_entry(link->reg);
    free(link);
}

void rrn_info_shutdown() {
    if (!categories)
        return;

    RRN_LIST_FREE(info_head, free_info_link);
    info_head = info_tail = NULL;
    rrn_freev(categories);
    categories = NULL;

    return;
}
